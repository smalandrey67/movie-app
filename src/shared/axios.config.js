import axios from "axios";

const API_KEY = process.env.REACT_APP_API_KEY;
const API_URL_V2 = process.env.REACT_APP_API_URL + "/api/v2.2";
const API_URL_V1 = process.env.REACT_APP_API_URL + "/api/v2.1";

const HEADERS_META = {
	"Content-Type": "application/json",
	"X-API-KEY": API_KEY,
};

export const $api_v2 = axios.create({
	baseURL: API_URL_V2,
	headers: HEADERS_META,
});

export const $api_v1 = axios.create({
	baseURL: API_URL_V1,
	headers: HEADERS_META,
});
