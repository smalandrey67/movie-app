import { useMemo } from "react";

const getLargeNumber = () => {
	console.log("tst");
	let j = 0;

	for (let i = 0; i < 100000000; i++) {
		j += i;
	}

	return j;
};

export function MemoExample() {
	const number = useMemo(() => getLargeNumber(), []);

	return <div>{number}</div>;
}
