import { useEffect, useState, useCallback } from "react";
import { FilmController } from "./controllers/films.controller";

import { Films } from "./components/business/Films/Films";
import { Header } from "./components/Layout/components/Header/Header";
import { Layout } from "./components/Layout/Layout";

import { useDebounce } from "./hooks/useDebounce";

export function Home() {
	const [films, setFilms] = useState([]);
	const [filmTerm, setFilmTerm] = useState("");

	const debouncedSearchTerm = useDebounce(filmTerm, 2000);

	useEffect(() => {
		if (!debouncedSearchTerm) return;

		const getFilmsByTerm = async () => {
			const updatedTerm = debouncedSearchTerm.toLowerCase().trim();

			if (!updatedTerm) return;

			const filmsData = await FilmController.getFilmsByTerm(updatedTerm);
			setFilms(filmsData.films);
		};

		getFilmsByTerm();
	}, [debouncedSearchTerm]);

	useEffect(() => {
		const getFilms = async () => {
			const filmsData = await FilmController.getFilms();
			setFilms(filmsData.items);
		};
		getFilms();
	}, []);

	const searchFilms = useCallback((event) => {
		setFilmTerm(event.target.value);
	}, []);

	return (
		<div className="app">
			<Layout>
				<Header searchFilms={searchFilms} />
				<Films films={films} />
			</Layout>
		</div>
	);
}
