import { $api_v1, $api_v2 } from "../shared/axios.config";

export const FilmService = {
	async getFilms() {
		const { data: films } = await $api_v2("/films");
		return films;
	},

	async getFilmInformation(filmId) {
		const { data: filmInformation } = await $api_v2(`/films/${filmId}`);
		return filmInformation;
	},

	async getFilmsByTerm(filmTerm) {
		const { data: films } = await $api_v1(`/films/search-by-keyword?keyword=${filmTerm}`);
		return films;
	},
};
