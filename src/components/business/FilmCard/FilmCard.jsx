import styles from "./FilmCard.module.scss";

export function FilmCard({ film, getFilmInformation }) {
	const currentFilmName = (film.nameOriginal ?? film.nameRu) || "No Film Name";
	const currentRating = (film.ratingImdb ?? film.ratingKinopoisk) || 0;

	return (
		<article className={styles.film} onClick={() => getFilmInformation(film.kinopoiskId)}>
			<div className={styles.filmImageWrapper}>
				<img src={film.posterUrlPreview} className={styles.filmImage} alt={film.nameOriginal} />
				<div className={styles.filmRating}>{currentRating}</div>
			</div>
			<h3 className={styles.filmTitle}>{currentFilmName}</h3>
		</article>
	);
}
