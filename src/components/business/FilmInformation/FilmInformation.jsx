import styles from "./FilmInformation.module.scss";

export function FilmInformation({ filmInformation }) {
	console.log(filmInformation);
	return (
		<div kye={filmInformation.kinopoiskId} className={styles.film}>
			<div className={styles.filmImageWrapper}>
				<img className={styles.filmImage} src={filmInformation.posterUrl} alt="logo" />
			</div>
			<div className={styles.filmDetails}>
				<h4 className={styles.filmTitle}>
					{filmInformation.nameRu} / {filmInformation.startYear}
				</h4>
				<div className={styles.filmBreadcrumbs}>
					{filmInformation.countries.map(({ country }) => (
						<div className={styles.filmBreadcrumb}>{country}</div>
					))}
				</div>
				<div className={styles.filmBreadcrumbs}>
					{filmInformation.genres.map(({ genre }) => (
						<div className={styles.filmBreadcrumb}>{genre}</div>
					))}
				</div>
				<p className={styles.filmDescription}>{filmInformation.description}</p>
			</div>
		</div>
	);
}
