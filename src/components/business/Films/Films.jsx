import { useState, memo } from "react";

import { FilmController } from "../../../controllers/films.controller";

import { FilmCard } from "../FilmCard/FilmCard";
import { FilmInformation } from "../FilmInformation/FilmInformation";

import { Modal } from "../../ui/Modal/Modal";
import { AppContainer } from "../../ui/AppContainer/AppContainer";

import styles from "./Films.module.scss";

export const Films = memo(({ films, count }) => {
	const [filmInformation, setFilmInformation] = useState({});
	const [isModalOpen, setIsModalOpen] = useState(false);

	const toggleModal = () => {
		setIsModalOpen((prevIsModalOpen) => !prevIsModalOpen);
	};

	const getFilmInformation = async (filmId) => {
		const filmInformationData = await FilmController.getFilmInformation(filmId);

		setFilmInformation(filmInformationData);
		toggleModal();
	};

	return (
		<AppContainer>
			{count}
			<div className={styles.films}>
				{films.length ? films.map((film) => <FilmCard getFilmInformation={getFilmInformation} film={film} key={film.kinopoiskId} />) : "Loading..."}
			</div>
			<Modal toggleModal={toggleModal} isModalOpen={isModalOpen}>
				{isModalOpen && <FilmInformation filmInformation={filmInformation} />}
			</Modal>
		</AppContainer>
	);
});
