import { Footer } from "./components/Footer/Footer";

export function Layout({ children }) {
	return (
		<>
			<main>{children}</main>
			<Footer />
		</>
	);
}
