import { memo } from "react";

import { AppContainer } from "../../../ui/AppContainer/AppContainer";
import { FilmSearch } from "./components/FilmSearch/FilmSearch";

import styles from "./Header.module.scss";

export const Header = memo(({ searchFilms }) => {
	console.log("render");

	return (
		<header className={styles.header}>
			<AppContainer>
				<div className={styles.headerWrapper}>
					<a href="/" className={styles.headerLogo}>
						Movie app
					</a>
					<FilmSearch searchFilms={searchFilms} />
				</div>
			</AppContainer>
		</header>
	);
});
