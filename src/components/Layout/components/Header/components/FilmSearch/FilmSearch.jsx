import { Input } from "../../../../../ui/Input/Input";

import styles from "./FilmSearch.module.scss";

export function FilmSearch({ searchFilms }) {
	return (
		<div className={styles.filmSearch}>
			<Input type="text" name="search-film" placeholder="search the film" onChange={searchFilms} />
		</div>
	);
}
