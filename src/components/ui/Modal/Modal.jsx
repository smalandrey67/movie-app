import clsx from "clsx";
import { RxCross1 } from "react-icons/rx";

import styles from "./Modal.module.scss";

export function Modal({ toggleModal, isModalOpen, children }) {
	return (
		<div
			className={clsx(styles.modal, {
				[styles.modalActive]: isModalOpen,
			})}
			onClick={toggleModal}
		>
			<div
				className={clsx(styles.modalBody, {
					[styles.modalBodyActive]: isModalOpen,
				})}
				onClick={(event) => event.stopPropagation()}
			>
				<div className={styles.modalHeader}>
					<h3 className={styles.modalTitle}>Information of film</h3>
					<button className={styles.modalClose} onClick={toggleModal}>
						<RxCross1 color="white" />
					</button>
				</div>
				<div className={styles.modalContent}>{children}</div>
			</div>
		</div>
	);
}
