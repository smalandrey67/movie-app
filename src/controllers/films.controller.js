import { FilmService } from "../services/films.service";

export const FilmController = {
	async getFilms() {
		try {
			const films = await FilmService.getFilms();
			return films;
		} catch (error) {
			console.error(error);
		}
	},
	async getFilmInformation(filmId) {
		try {
			const filmInformation = await FilmService.getFilmInformation(filmId);
			return filmInformation;
		} catch (error) {
			console.error(error);
		}
	},

	async getFilmsByTerm(filmTerm) {
		try {
			const films = await FilmService.getFilmsByTerm(filmTerm);
			return films;
		} catch (error) {
			console.error(error);
		}
	},
};
