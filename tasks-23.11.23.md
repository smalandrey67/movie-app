# Task: 1.1
--------------- 
Create reusable user component. This component might accept such props as ( userAvatar, userName, userEmail ). Use scss. Render three different users.
--------------- 

# Task: 1.2
--------------- 
Create counter with ability to increase and decrease value.
--------------- 

# Task: 1.3
--------------- 
Create cars array. Render him on the page. Once clicked on the car remove her. Similar as we did on the lesson with user.
--------------- 